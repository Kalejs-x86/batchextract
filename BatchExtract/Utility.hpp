#pragma once
#include <sstream>

#include "TypeDef.hpp"

static std::vector<std::string> SplitString(const std::string& str, char delim = ' ')
{
	std::vector<std::string> result;
	std::stringstream ss(str);
	std::string token;
	while (std::getline(ss, token, delim)) {
		result.push_back(token);
	}
	return result;
}

bool SigScan(ByteArray Data, unsigned long Offset, const char *Signature, unsigned long &Pos)
{
	// Convert Signature from string to hex values
	auto szSigBytes = SplitString(Signature);
	ByteArray SigBytes;
	for (auto szByte : szSigBytes)
	{
		byte Byte = std::stoi(szByte, 0, 16);
		SigBytes.push_back(Byte);
	}

	if (SigBytes.size() == 0 || Data.size() == 0)
		return false;

	// Iterate the supplied data and check for a match
	for (int DataIndex = Offset; DataIndex < Data.size(); ++DataIndex)
	{
		byte CurByte = Data[DataIndex];

		// Found the first matching byte
		if (CurByte == SigBytes[0])
		{
			bool AllMatch = true;
			// Iterate this section of data for the sig
			for (int SigIndex = 1; SigIndex < SigBytes.size(); ++SigIndex)
			{
				if (DataIndex + SigIndex >= Data.size()) // Ran out of Data to compare
					return false;

				byte NextByte = Data[DataIndex + SigIndex];
				if (NextByte != SigBytes[SigIndex])
				{
					// Not a match, dont stop the function, just move to the next byte
					AllMatch = false;
					break;
				}
			}

			// Every byte matched the supplied signature
			if (AllMatch)
			{
				Pos = DataIndex;
				return true;
			}
		}
	}

	return false;
}