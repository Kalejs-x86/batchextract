#pragma once
#include <vector>

#include "TypeDef.hpp"

namespace Bat2Exe
{
	// Where the main executable ends and the embedded data starts
	// Usually 0x14000 except when built with the 'Start Invisible' option
	const unsigned DataOffset = 0x12000;
	// Marks the start of batch data
	const char BeginSignature[] = "44 46 44 48 45 52 47 44 43 56";
	// Byte count of BeginSignature
	const unsigned short BeginSigSize = 10;
	//Marks the end of batch data
	const char EndSignature[] = "44 46 44 48 45 52 47 47 5A 56";
	// Byte count of EndSignature
	const unsigned short EndSigSize = 10;
	// String used by the decryption algorithm 
	const char MagicNumber[] = "01337913379";

	ByteArray Decrypt(ByteArray Data, long Start = 0, long End = 0)
	{
		int Index = 0;
		ByteArray Decrypted;

		if (End == 0) End = Data.size();

		for (int i = Start; i < End; ++i)
		{
			byte Byte = Data[i];

			if (++Index > 0xA)
				Index = 1;

			Byte -= MagicNumber[Index];

			if (Byte > 0xFF)
				Byte -= 0x100;

			Byte += -(Byte < 0) & 0x100;

			if (++Index > 0xA)
				Index = 1;

			Byte -= MagicNumber[Index];

			if (Byte > 0xFF)
				Byte -= 0x100;

			Byte += -(Byte < 0) & 0x100;

			if (--Index < 1)
				Index = 10;

			Decrypted.push_back(Byte);
		}
		return Decrypted;
	}
}